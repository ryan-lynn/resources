## Resources

---

#### Docker

- [Intro to Docker](http://blog.brew.com.hk/introduction-to-docker/)
- [Useful Commands](https://blog.pavelsklenar.com/10-useful-docker-commands-tip-tricks/)
- [Docker Comics](https://consolia-comic.com/comics/containers-and-docker)

---

#### Git

- [Git - no deep shit!](https://rogerdudler.github.io/git-guide/)
- [Git cheatsheet](https://ndpsoftware.com/git-cheatsheet.html)
- [Git Flight Rules](https://github.com/k88hudson/git-flight-rules)

---

#### Go

- [Go by Example](https://gobyexample.com/)

---

#### JavaScript

- [Algorithms and Data Structures](https://github.com/trekhleb/javascript-algorithms)
- [ES6 Cheatsheet](http://www.developer-cheatsheets.com/es6)
- [33 concepts every JavaScript developer should know.](https://github.com/leonardomso/33-js-concepts)

---

#### Interview Prep

- [30 Seconds of Interviews](https://30secondsofinterviews.org/)
- [10 Interview Questions Every JS Dev Should Know](https://medium.com/javascript-scene/10-interview-questions-every-javascript-developer-should-know-6fa6bdf5ad95)
- [Jennifer Best Trello Board of Resources](https://trello.com/b/PMZoS0pC/the-master-resource-list-web-dev)

---

#### Misc

- [30 Seconds of Code](https://30secondsofcode.org/)

---

#### PGP

- [MIT PGP Key Server](https://pgp.mit.edu/)
- [Keysigning - Debian Wiki](https://wiki.debian.org/Keysigning)
- [How To Use GPG on the Command Line](https://blog.ghostinthemachines.com/2015/03/01/how-to-use-gpg-command-line/)
- [GnuPG - Support](https://www.gnupg.org/documentation/index.en.html)

---

#### React

- [React Cheatsheet](http://www.developer-cheatsheets.com/react)
- [HOCs](https://tylermcginnis.com/react-higher-order-components/)

---

#### Terminal

- [CLI Improved](https://remysharp.com/2018/08/23/cli-improved)
- [Tmux Shortcuts](https://gist.github.com/MohamedAlaa/2961058)
- [Badassify your terminal](https://jilles.me/badassify-your-terminal-and-shell/)

---

#### Webpack

- [Config generator](https://webpack.jakoblind.no/)

---
